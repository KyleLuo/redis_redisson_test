package org.example.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@Slf4j
public class JacksonUtil {

    @Autowired
    private static ObjectMapper mapper;

    public static <T> T anyToBean(Object obj, Class<T> clazz) {
        return mapper.convertValue(obj, clazz);
    }

    public static String beanToJson(Object bean) {
        try {
            return mapper.writeValueAsString(bean);
        } catch (JsonProcessingException e) {
            log.error("对象转JSON字符串失败", e);
            throw new RuntimeException("对象转JSON字符串失败");
        }
    }

    /**
     * bean 转 map
     *
     * @param bean 对象
     * @return Map<String, Object>
     */
    public static Map<String, Object> beanToMap(Object bean) {
        return mapper.convertValue(bean, new TypeReference<Map<String, Object>>() {
        });
    }

    /**
     * bean 转 List
     *
     * @param bean 对象
     * @return
     */
    public static List<Object> beanToList(Object bean) {
        return mapper.convertValue(bean, new TypeReference<List<Object>>() {
        });
    }

    public static <T> T jsonToBean(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error(json, e);
            throw new RuntimeException("JSON字符串转对象失败");
        }
    }

    public static <T> T jsonToBean(String json, TypeReference<T> type) {
        try {
            return mapper.readValue(json, type);
        } catch (IOException e) {
            log.error(json, e);
            throw new RuntimeException("JSON字符串转对象失败");
        }
    }

    public static Map<String, Object> jsonToMap(String json) {
        return jsonToBean(json, new TypeReference<Map<String, Object>>() {
        });
    }

    public static <K, V> Map<K, V> jsonToMap(String json, Class<K> kClass, Class<V> vClass) {
        try {
            return mapper.readValue(json, mapper.getTypeFactory().constructMapType(Map.class, kClass, vClass));
        } catch (IOException e) {
            log.error(json, e);
            throw new RuntimeException("JSON字符串转Map失败");
        }
    }

    public static List<Object> jsonToList(String json) {
        return jsonToBean(json, new TypeReference<List<Object>>() {
        });
    }

    public static <T> List<T> jsonToList(String json, Class<T> eClass) {
        try {
            return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, eClass));
        } catch (IOException e) {
            log.error(json, e);
            throw new RuntimeException("JSON字符串转List失败");
        }
    }

    public static <T> Set<T> jsonToSet(String json, Class<T> eClass) {
        try {
            return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(Set.class, eClass));
        } catch (IOException e) {
            log.error(json, e);
            throw new RuntimeException("JSON字符串转List失败");
        }
    }

    /**
     * 校验传进来的字符串是否为json格式
     *
     * @param jsonInString
     * @return
     */
    public final static boolean isJSONValid(String jsonInString) {
        try {
            JsonNode jsonNode = mapper.readTree(jsonInString);
            return jsonNode != null && jsonNode.size() > 0;
        } catch (IOException e) {
            jsonInString = jsonInString.replaceAll("'", "\"");
            try {
                mapper.readTree(jsonInString);
                return true;
            } catch (IOException e1) {
                return false;
            }
        }
    }

}