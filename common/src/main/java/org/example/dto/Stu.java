package org.example.dto;

/**
 * @author kyle
 * @date 2020-09-04
 */
public class Stu {
    private String name;
    private int age;

    public static StuBuider builder() {
        return new StuBuider();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static class StuBuider {
        private String name;
        private int age;


        private StuBuider() {
        }

        public StuBuider name(String name) {
            this.name = name;
            return this;
        }

        public StuBuider age(int age) {
            this.age = age;
            return this;
        }

        public Stu build() {
            Stu stu = new Stu();
            stu.setName(name);
            stu.setAge(age);
            return stu;
        }
    }
}
