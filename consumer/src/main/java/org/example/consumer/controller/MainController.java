package org.example.consumer.controller;

import org.example.consumer.service.ConsumeService;
import lombok.extern.slf4j.Slf4j;
import org.example.consumer.service.NewConsumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 控制器
 *
 * @since 2022/7/12
 **/
@RestController
@Slf4j
@RequestMapping("/consumer")
public class MainController {

    @Autowired
    private ConsumeService consumeService;

    @Autowired
    private NewConsumeService newConsumeService;

    @GetMapping("/run")
    public void run() {
        consumeService.run();
    }

    @GetMapping("/run2")
    public void run2() {
        newConsumeService.run();
    }

}
