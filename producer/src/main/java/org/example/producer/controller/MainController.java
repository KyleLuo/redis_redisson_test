package org.example.producer.controller;

import org.example.producer.service.ProduceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 控制器
 *

 * @since 2022/7/12
 **/
@RestController
@Slf4j
@RequestMapping("/producer")
public class MainController {

    @Autowired
    private ProduceService produceService;

    @GetMapping("/run")
    public void run() {
        produceService.run();
    }

}
